import urllib
import urllib2
import os
from bs4 import BeautifulSoup

print "fetching HTML"

url = 'http://www.amazon.in/gp/registry/search.html/ref=cm_wl_search_go?ie=UTF8&type=wishlist'
values = {'field-name' : 'sakuag333@gmail.com'}   

user_agent = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22"
headers = {'User-Agent' : user_agent}

"""
data = urllib.urlencode(values)
response = urllib2.urlopen(url, data)
"""

data = urllib.urlencode(values)
req = urllib2.Request(url, data)
response = urllib2.urlopen(req)

page = response.read()
page = os.linesep.join([s for s in page.splitlines() if s])

print "Parsing HTML"

parsed_html = BeautifulSoup(page,"lxml")


#parsed_html = BeautifulSoup(open("./amazonWishListHTML.html"),"lxml");
f1 = open("./out.txt","w");
#print>>f1 , page

#For Amazon.in
print "Processing with .in parser"
reqDiv = parsed_html.find_all("tbody",{"class":"itemWrapper"})
for t in reqDiv:
    allImg = t.find_all("img")
    print>>f1, allImg[0].get('src')
    spanName = t.find_all("span",{"class":"small productTitle"})
    print>>f1, spanName[0].get_text().encode('utf-8')
    t1 = t.find_all("div",{"class":"lineItemGroup"})
    #print>>f1, t1[0]
    #spanBy = t1[0].find_all("span",{"class":"authorPart"})
    #print>>f1, spanBy[0].get_text().encode('utf-8')
    spanPrice = t.find_all("span",{"class":"wlPriceBold"})
    if len(spanPrice) == 0:
        spanPrice = t.find_all("span",{"class":"wlPrice"})
    if len(spanPrice) > 0:    
    	print>>f1, spanPrice[0].get_text().encode('utf-8')
    allA = t.find_all("a")
    print>>f1, allA[0].get('href')
    print>>f1, '\n'

if len(reqDiv) == 0 :
	#For Amazon.com
	print "Processing with .com parser"
	reqDiv = parsed_html.find_all("div",{"class":"a-fixed-left-grid   a-spacing-large"})
	for t in reqDiv:
	    allImg = t.find_all("img")
	    print>>f1, allImg[0].get('src')           #image of item
	    divReq = t.find_all("div",{"class":"a-column a-span8 g-span12when-narrow"})
	    allSpan = divReq[0].find_all("div")
	    print>>f1, allSpan[0].get_text().encode('utf-8')    #name of item
	    priceSpan = divReq[0].find_all("span",{"class":"a-size-medium a-color-price"})
	    print>>f1, priceSpan[0].get_text().encode('utf-8')    # price of item
	    allA = t.find_all("a")
	    print>>f1, allA[0].get('href').encode('utf-8')      # link of item
	    print>>f1, '\n'

print len(reqDiv)
#print>>f1, page





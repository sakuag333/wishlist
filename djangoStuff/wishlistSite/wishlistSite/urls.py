from django.conf.urls import patterns, include, url
from wishlistSite.views import hello,helloDjango,fbconnect,channel

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
			url(r'^hello/$',hello),
			url(r'^test/$',helloDjango),
			url(r'^channel/$',channel),
			url(r'login',fbconnect),
    # Examples:
    # url(r'^$', 'wishlistSite.views.home', name='home'),
    # url(r'^wishlistSite/', include('wishlistSite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)

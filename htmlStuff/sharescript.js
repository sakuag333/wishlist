(function () {
    (function () {
        var k = document.createElement("div"),
            b = !1;
        ["Moz", "Webkit", "ms"].forEach(function (l) {
            b |= l + "Animation" in k.style
        });
        return !!b
    })();
    document.createElement("input").setAttribute("type", "file");
    window.navigator.userAgent.match(/Macintosh/);
    (function () {
        var k = document.createElement("div"),
            b = !1;
        ["Moz", "Webkit", "ms"].forEach(function (l) {
            b |= l + "Transition" in k.style
        });
        return !!b
    })();
    $(s);

    function s() { };
    (function () {
        function k() {
            $("body").mousemove(function (a) {
                n = a.clientX;
                p = a.clientY
            });
            $(window).on("blur", function () {
                p = n = null
            }).on("resize", function () {
                a && a.parentNode && a.parentNode.removeChild(a);
                b()
            });
            b()
        }
        function b() {
            var d = null == f ? !0 : !1;
            a = document.createElement("canvas");
            a.width = $(window).width();
            a.height = $(window).height();
            a.style.opacity = "0.95";
            $("body").append(a);
            f = document.createElement("canvas");
            f.width = $(window).width();
            f.height = $(window).height();
            a.getContext && a.getContext("2d") && (m = a.getContext("2d"), e = f.getContext("2d"), e.lineCap = "round", e.shadowColor = "#000000", e.shadowBlur = 30, h = new Image, h.src = "./image_wishlist.jpg", h.onload = function () {
                d && l()
            })
        }
        function l() {
            window.g(l);
            var b = Date.now();
            n && d.splice(0, 0, {
                time: b,
                x: n,
                y: p
            });
            e.clearRect(0, 0, f.width, f.height);
            for (var c = 1; c < d.length; c++) {
                if (b - d[c].time > q) {
                    d.splice(c, d.length);
                    break
                }
                var k = Math.sqrt(Math.pow(d[c].x - d[c - 1].x, 2) + Math.pow(d[c].y - d[c - 1].y, 2));
                e.strokeStyle = "rgba(0,0,0," + Math.max(1 - (b - d[c].time) / q, 0) + ")";
                e.lineWidth = r + Math.max(1 - k / t, 0) * (u - r);
                e.beginPath();
                e.moveTo(d[c - 1].x, d[c - 1].y);
                e.lineTo(d[c].x, d[c].y);
                e.stroke()
            }
            b = a.width;
            c = a.width / h.naturalWidth * h.naturalHeight;
            c < a.height && (c = a.height, b = a.height / h.naturalHeight * h.naturalWidth);
            m.drawImage(h, 0, 0, b, c);
            m.globalCompositeOperation = "destination-in";
            m.drawImage(f, 0, 0);
            m.globalCompositeOperation = "source-over"
        }
        var q = 1E3,
            r = 25,
            u = 100,
            t = 50,
            a, f, m, e, h, n = null,
            p = null,
            d = [];
        "createTouch" in document || $(k);
        window.g = function () {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (a) {
                window.setTimeout(a, 1E3 / 60)
            }
        } ()
    })();
})();
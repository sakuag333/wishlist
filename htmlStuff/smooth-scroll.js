// Document ready shorthand statement
  //  $("#learnMore").on("click",M);
   // function M(){$("body,html").animate({scrollTop:$("#hero").height()},700);return!1}
   // function M() {$("body,html").animate(alert("sa");{scrollTop:$("#main_image").height()},700);return!1}

// Document ready shorthand statement
    $(function() {
      // Select link by id and add click event
      $('#learnMore').click(function() {

        // Animate Scroll to #bottom
        $('html,body').animate({
          scrollTop:$("#main_image").height() }, // Tell it to scroll to the top #bottom
          '700' // How long scroll will take in milliseconds
        );

        // Prevent default behavior of link
        return false;
      });
    });
